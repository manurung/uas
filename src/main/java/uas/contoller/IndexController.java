package uas.contoller;


import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import uas.dao.AkunDao;
import uas.model.Akun;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class IndexController {

	private AkunDao aDao;
	private Akun akunLogin;

	@Autowired
	public void setuDao(AkunDao aDao) {
		this.aDao = aDao;
	}
	
	@RequestMapping("/uas")
	public String index(Model model) {
		model.addAttribute("akun", new Akun());
		model.addAttribute("akunLogin",new Akun());
		return "index";
	}


	@RequestMapping(value = "/createAkun", method = RequestMethod.POST)
	public String saveOrUpdateAkun(Model model, Akun akun) {
		model.addAttribute("akun", aDao.saveOrUpdate(akun));
		return "redirect:/uas";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(HttpServletRequest request) {
		String username = request.getParameter("user");
		String password = request.getParameter("pass");
		if(aDao.login(username, password) != null){
			request.getSession().setAttribute("akunLogin", aDao.login(username, password));
			akunLogin = (Akun) request.getSession().getAttribute("akunLogin");
			if (akunLogin.getRole().equals("Mahasiswa")) {
				return "redirect:/uas/mahasiswa";
			} else if (akunLogin.getRole().equals("Duktek")) {
				return "redirect:/uas/duktek";
			} else if (akunLogin.getRole().equals("Satpam")) {			
				return "redirect:/uas/satpam";
			} 
		}		
		return "redirect:/izinlaptop";
	}
	@RequestMapping("/logout")
	public String logout(HttpServletRequest request){
		request.getSession().removeAttribute("akunLogin");
		return "redirect:/uas";
	}

}
