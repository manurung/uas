package uas.dao;

import java.util.List;
import uas.model.Akun;

public interface AkunDao {
	List<Akun> getAllAkun();
	Akun saveOrUpdate(Akun akun);
	Akun login(String username, String password);
}
